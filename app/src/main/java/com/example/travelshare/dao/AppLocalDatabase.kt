package com.example.travelshare.dao

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.travelshare.base.MyApplication
import com.example.travelshare.converters.JourneyConverter
import com.example.travelshare.model.Journey
import com.example.travelshare.model.Post
import com.example.travelshare.model.User

@Database(entities = [Journey::class, User::class, Post::class], version = 8)
@TypeConverters(JourneyConverter::class)
abstract class AppLocalDbRepository : RoomDatabase() {
    abstract fun journeyDao(): JourneyDao
    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao
}

object AppLocalDatabase {

    val db: AppLocalDbRepository by lazy {

        val context = MyApplication.Globals.appContext
            ?: throw IllegalStateException("Application context not available")

        Room.databaseBuilder(
            context,
            AppLocalDbRepository::class.java,
            "dbFileName.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }
}