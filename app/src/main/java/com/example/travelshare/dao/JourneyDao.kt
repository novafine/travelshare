package com.example.travelshare.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.travelshare.model.Journey

@Dao
interface JourneyDao {
    @Query("SELECT * FROM Journey")
    fun getAll(): LiveData<List<Journey>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg journey: Journey)

    @Query("DELETE FROM Journey WHERE id =:journeyId")
    fun deleteByJourneyId(journeyId: String)

    @Query("SELECT * FROM Journey WHERE id =:id")
    fun getJourneyById(id: String): LiveData<Journey>

    @Query("SELECT * FROM Journey WHERE userId =:userId")
    fun getJourneysByUserId(userId: String): LiveData<List<Journey>>
}