package com.example.travelshare.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.travelshare.model.Journey
import com.example.travelshare.model.Post

@Dao
interface PostDao {
    @Query("SELECT * FROM Post WHERE id IN (:postsIds)")
    fun getPostsByIds(postsIds: ArrayList<String>): LiveData<List<Post>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg post: Post)

    @Delete
    fun delete(post: Post)
}