package com.example.travelshare.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.travelshare.model.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg user: User)

    @Delete
    fun delete(user: User)

    @Query("SELECT * FROM User WHERE id =:id")
    fun getUserById(id: String): LiveData<User>

    @Query("SELECT * FROM User")
    fun getAllUsers(): LiveData<List<User>>
}