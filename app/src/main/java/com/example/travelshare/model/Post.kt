package com.example.travelshare.model

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.travelshare.base.MyApplication

@Entity
data class Post(
    @PrimaryKey val id: String,
    val country: String,
    val title: String,
    val description: String,
    val imageURI: String,
    val journeyId: String,
    val lastUpdated: Long,
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readLong() ?: 0
    )

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(country)
        dest.writeString(title)
        dest.writeString(description)
        dest.writeString(imageURI)
        dest.writeString(journeyId)
        dest.writeLong(lastUpdated)
    }

    companion object CREATOR : Parcelable.Creator<Post> {
        override fun createFromParcel(parcel: Parcel): Post {
            return Post(parcel)
        }

        override fun newArray(size: Int): Array<Post?> {
            return arrayOfNulls(size)
        }

        const val GET_LAST_UPDATED = "get_last_updated"

        var lastUpdated: Long
            get() {
                return MyApplication.Globals
                    .appContext?.getSharedPreferences("TAG", Context.MODE_PRIVATE)
                    ?.getLong(GET_LAST_UPDATED, 0) ?: 0L
            }
            set(value) {
                MyApplication.Globals
                    ?.appContext
                    ?.getSharedPreferences("TAG", Context.MODE_PRIVATE)?.edit()
                    ?.putLong(GET_LAST_UPDATED, value)?.apply()
            }

        fun fromJson(json: Map<String, Any>): Post {
            val id = json["id"] as? String ?: ""
            val country = json["country"] as? String ?: ""
            val title = json["title"] as? String ?: ""
            val description = json["description"] as? String ?: ""
            val imageURI = json["imageURI"] as? String ?: ""
            val journeyId = json["journeyId"] as? String ?: ""
            val lastUpdated = json["lastUpdated"] as? Long ?: 0L

            return Post(id, country, title, description, imageURI, journeyId, lastUpdated)
        }
    }

    val json: HashMap<String, Any>
        get() {
            return hashMapOf(
                "id" to this.id,
                "country" to this.country,
                "title" to this.title,
                "description" to this.description,
                "imageURI" to this.imageURI,
                "journeyId" to this.journeyId,
                "lastUpdated" to this.lastUpdated
            )
        }
}
