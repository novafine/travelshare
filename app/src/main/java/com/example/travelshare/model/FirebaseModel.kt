package com.example.travelshare.model

import android.content.Context
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.UploadTask
import com.google.firebase.storage.ktx.storage
import java.util.UUID

object FirebaseModel {
    private val db by lazy { Firebase.firestore }
    private val storage = Firebase.storage
    private const val POSTS_COLLECTION = "posts"
    private const val JOURNEYS_COLLECTION = "journeys"
    private const val USERS_COLLECTION = "users"

    init {
        val settings = FirebaseFirestoreSettings.Builder().setPersistenceEnabled(false).build()

        db.firestoreSettings = settings
    }

    fun uploadImageToFirebase(
        imageUri: Uri,
        onSuccessCallback: ((imagePath: String) -> Unit)? = null,
    ) {
        val fileName = UUID.randomUUID().toString() + ".jpg"
        val refStorage = storage.reference.child("images/$fileName")

        refStorage.putFile(imageUri)
            .addOnSuccessListener(OnSuccessListener<UploadTask.TaskSnapshot> { taskSnapshot ->
                taskSnapshot.storage.downloadUrl.addOnSuccessListener {
                    val imagePath = it.toString() // TODO: Save to DB with new user
                    onSuccessCallback?.let { it(imagePath) }
                }
            }).addOnFailureListener(OnFailureListener { e ->
                Log.e("storage", e.message.toString())
            })
    }

    private fun addJourneyIfNotExists(
        journeyId: String,
        userId: String,
        country: String,
        callback: () -> Unit,
    ) {
        db.collection(JOURNEYS_COLLECTION).document(journeyId).get().addOnSuccessListener {
            if (it.data == null) {
                val journey = Journey(
                    journeyId,
                    userId,
                    country,
                    arrayListOf<String>(),
                    System.currentTimeMillis()
                )
                db.collection(JOURNEYS_COLLECTION).document(journeyId)
                    .set(journey.json).addOnSuccessListener {
                        Log.d("addJourney", "add journey $journeyId successfully")
                        callback()
                    }.addOnFailureListener { exception ->
                        Log.e("addJourney", "failed adding journey $journeyId", exception)
                    }
            } else {
                Log.d("addJourney", "journey $journeyId already exists")
                callback()
            }
        }.addOnFailureListener {
            Log.e("addJourney", "failed adding journey $journeyId", it)
        }
    }

    private fun addPostToJourney(
        journeyId: String,
        postId: String,
        country: String,
        callback: () -> Unit,
    ) {
        if (FirebaseAuth.getInstance().currentUser != null) {
            addJourneyIfNotExists(
                journeyId,
                FirebaseAuth.getInstance().currentUser!!.uid,
                country
            ) {
                val update = hashMapOf(
                    "posts" to FieldValue.arrayUnion(postId),
                    "lastUpdated" to System.currentTimeMillis()
                )
                db.collection(JOURNEYS_COLLECTION).document(journeyId)
                    .update(update).addOnSuccessListener {
                        callback()
                    }.addOnFailureListener { exception ->
                        Log.e("addPost", "failed adding post to journey", exception)
                    }
            }
        } else {
            Log.e("addPost", "failed adding post to journey, user is not logged in")
        }
    }

    fun addPost(
        country: String,
        title: String,
        description: String,
        imageURI: String,
        callback: () -> Unit,
    ) {
        val postDocRef = db.collection(POSTS_COLLECTION).document()
        val journeyId = this.getJourneyId(
            country,
            Firebase.auth.currentUser!!.uid
        )
        val post = Post(
            postDocRef.id,
            country,
            title,
            description,
            imageURI,
            journeyId,
            System.currentTimeMillis()
        )
        postDocRef.set(post.json).addOnSuccessListener {
            if (FirebaseAuth.getInstance().currentUser?.uid != null) {
                this.addPostToJourney(
                    journeyId,
                    postDocRef.id,
                    post.country,
                    callback
                )
            } else {
                Log.e("addPost", "failed adding post to journey, user is not logged in")
            }
        }.addOnFailureListener {
            Log.e("addPost", "failed creating post", it)
        }
    }

    fun deletePost(postId: String, callback: () -> Unit) {
        db.collection(POSTS_COLLECTION).document(postId).delete().addOnSuccessListener {
            callback()
        }.addOnFailureListener {
            Log.e("deletePost", "post $postId failed", it)
        }
    }

    fun updatePost(
        postId: String,
        title: String,
        description: String,
        imageURI: String,
        callback: () -> Unit,
    ) {
        val update: Map<String, Any> = hashMapOf(
            "title" to title,
            "description" to description,
            "imageURI" to imageURI,
            "lastUpdated" to System.currentTimeMillis()
        )
        db.collection(POSTS_COLLECTION).document(postId).update(update).addOnSuccessListener {
            callback()
        }.addOnFailureListener {
            Log.e("deletePost", "post $postId failed", it)
        }
    }

    fun removePostFromJourney(postId: String, journeyId: String, callback: () -> Unit) {
        val journeyRef = db.collection(JOURNEYS_COLLECTION)
        val update = hashMapOf(
            "posts" to FieldValue.arrayRemove(postId),
            "lastUpdated" to System.currentTimeMillis()
        )
        journeyRef.document(journeyId).update(update).addOnSuccessListener {
            journeyRef.document(journeyId).get().addOnCompleteListener {
                if (it.isSuccessful) {
                    it.result.data?.let { journey ->
                        val posts = Journey.fromJson(journey).posts
                        if (posts.isEmpty()) {
                            journeyRef.document(journeyId).delete().addOnSuccessListener {
                                // TODO: Remove from local DB too
                                Log.d(
                                    "deleteJourney",
                                    "deleted empty journey $journeyId successfully"
                                )
                                callback()
                            }.addOnFailureListener { exception ->
                                Log.e(
                                    "deleteJourney",
                                    "failed deleting empty journey $journeyId",
                                    exception
                                )
                            }
                        } else {
                            callback()
                        }
                    }
                }
            }.addOnFailureListener {
                Log.e("deletePost", "remove post $postId from journey $journeyId failed", it)
            }
        }
    }

    private fun getJourneyId(country: String, userId: String): String {
        return "$userId$country"
    }

    fun addUserToDB(user: User, context: Context, callback: () -> Unit) {
        isUsernameExists(user.username) { exists ->
            if (!exists) {
                db.collection(USERS_COLLECTION).document(user.id).set(user.json)
                    .addOnSuccessListener {
                        callback()
                    }
                    .addOnFailureListener { exception ->
                        Log.e("addUser", "failed setting user", exception)
                        showToast(context, "Failed to add user")
                    }
            } else {
                showToast(context, "Username is already taken")
            }
        }
    }

    fun isUsernameExists(username: String, callback: (Boolean) -> Unit) {
        db.collection(USERS_COLLECTION)
            .whereEqualTo("username", username)
            .get()
            .addOnSuccessListener { querySnapshot ->
                callback(!querySnapshot.isEmpty)
            }
            .addOnFailureListener { exception ->
                Log.e("isUsernameExists", "failed to check username availability", exception)
                callback(false)
            }
    }

    fun updateUserUsername(
        userId: String,
        newUsername: String,
        context: Context,
        callback: (User?) -> Unit
    ) {
        if (newUsername == "") {
            showToast(context, "username cannot be empty")
            callback(null)
        } else {
            isUsernameExists(newUsername) { exists ->
                if (!exists) {
                    val userRef = db.collection(USERS_COLLECTION).document(userId)
                    userRef.update(
                        mapOf(
                            "username" to newUsername,
                            "lastUpdated" to System.currentTimeMillis()
                        )
                    )
                        .addOnSuccessListener {
                            userRef.get().addOnSuccessListener { documentSnapshot ->
                                val updatedUser = documentSnapshot.toObject(User::class.java)
                                callback(updatedUser)
                                Log.i("updateUser", "Succeeded to update user")
                            }.addOnFailureListener { e ->
                                Log.e("updateUser", "Failed to get updated user", e)
                                callback(null)
                            }
                        }
                        .addOnFailureListener { e ->
                            Log.e("updateUser", "Failed to update user", e)
                            callback(null)
                        }
                } else {
                    showToast(context, "Username is already taken")
                    callback(null)
                }
            }
        }
    }

    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


    fun updateUserProfilePic(userId: String, newImagePath: String, callback: (User?) -> Unit) {
        val userRef = db.collection(USERS_COLLECTION).document(userId)
        userRef.update(
            mapOf(
                "imagePath" to newImagePath,
                "lastUpdated" to System.currentTimeMillis()
            )
        )
            .addOnSuccessListener {
                userRef.get().addOnSuccessListener { documentSnapshot ->
                    val updatedUser = documentSnapshot.toObject(User::class.java)
                    callback(updatedUser)
                    Log.i("updateUser", "Succeeded to update user")
                }.addOnFailureListener { e ->
                    Log.e("updateUser", "Failed to get updated user", e)
                    callback(null)
                }
            }
            .addOnFailureListener { e ->
                Log.e("updateUser", "Failed to update user", e)
            }
    }

    fun getAllPostsSince(since: Long, callback: (ArrayList<Post>) -> Unit) {
        db.collection(POSTS_COLLECTION)
            .whereGreaterThanOrEqualTo("lastUpdated", since).get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val posts = ArrayList<Post>()
                    for (json in it.result) {
                        posts.add((Post.fromJson(json.data)))
                    }
                    callback(posts)
                } else {
                    callback(arrayListOf<Post>())
                }
            }
    }

    fun getAllJourneysSince(since: Long, callback: (ArrayList<Journey>) -> Unit) {
        db.collection(JOURNEYS_COLLECTION)
            .whereGreaterThanOrEqualTo("lastUpdated", since).get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val journeys = ArrayList<Journey>()
                    for (json in it.result) {
                        journeys.add((Journey.fromJson(json.data)))
                    }
                    callback(journeys)
                } else {
                    callback(arrayListOf<Journey>())
                }
            }
    }

    fun getAllUsersSince(since: Long, callback: (ArrayList<User>) -> Unit) {
        db.collection(USERS_COLLECTION)
            .whereGreaterThanOrEqualTo("lastUpdated", since).get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val users = ArrayList<User>()
                    for (json in it.result) {
                        users.add((User.fromJson(json.data)))
                    }
                    callback(users)
                } else {
                    callback(arrayListOf<User>())
                }
            }
    }

    fun getUserById(id: String, callback: (User) -> Unit) {
        db.collection(USERS_COLLECTION).document(id).get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val user = it.result.data?.let { data -> User.fromJson(data) }
                    if (user != null) {
                        callback(user)
                    }
                }
            }
    }
}