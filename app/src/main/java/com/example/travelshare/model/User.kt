package com.example.travelshare.model

import android.content.Context
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.travelshare.base.MyApplication

@Entity
data class User(
    @PrimaryKey val id: String,
    val username: String,
    val bio: String,
    val imagePath: String,
    val lastUpdated: Long,
) {

    constructor() : this("", "", "", "",0)
    companion object {
        const val GET_LAST_UPDATED = "get_last_updated"

        var lastUpdated: Long
            get() {
                return MyApplication.Globals
                    .appContext?.getSharedPreferences("TAG", Context.MODE_PRIVATE)
                    ?.getLong(GET_LAST_UPDATED, 0) ?: 0L
            }
            set(value) {
                MyApplication.Globals
                    ?.appContext
                    ?.getSharedPreferences("TAG", Context.MODE_PRIVATE)?.edit()
                    ?.putLong(GET_LAST_UPDATED, value)?.apply()
            }

        fun fromJson(json: Map<String, Any>): User {
            val id = json["id"] as? String ?: ""
            val username = json["username"] as? String ?: ""
            val bio = json["bio"] as? String ?: ""
            val imagePath = json["imagePath"] as? String ?: ""
            val lastUpdated = json["lastUpdated"] as? Long ?: 0L

            return User(id, username, bio, imagePath, lastUpdated)
        }
    }

    val json: HashMap<String, Any>
        get() {
            return hashMapOf(
                "id" to this.id,
                "username" to this.username,
                "bio" to this.bio,
                "imagePath" to this.imagePath,
                "lastUpdated" to this.lastUpdated
            )
        }
}


