package com.example.travelshare.model

data class Country (
    val name: CountryName
)

data class CountryName (
    val common: String
)