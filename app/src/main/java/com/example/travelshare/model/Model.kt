package com.example.travelshare.model

import android.content.Context
import android.net.Uri
import android.os.Looper
import androidx.core.os.HandlerCompat
import androidx.lifecycle.LiveData
import com.example.travelshare.dao.AppLocalDatabase
import java.util.concurrent.Executors

class Model private constructor() {
    private val firebaseModel = FirebaseModel
    private val localDB = AppLocalDatabase.db
    private var executor = Executors.newSingleThreadExecutor()
    private var mainHandler = HandlerCompat.createAsync(Looper.getMainLooper())

    companion object {
        val instance: Model = Model()
    }

    fun getAllJourneys(): LiveData<List<Journey>> {
        refreshAllJourneys()
        refreshAllUsers()
        refreshAllPosts()
        return localDB.journeyDao().getAll()
    }

    fun getJourneysByUserId(userId: String): LiveData<List<Journey>> {
        refreshAllJourneys()
        return localDB.journeyDao().getJourneysByUserId(userId)
    }

    fun getUserById(id: String): LiveData<User> {
        refreshAllUsers()
        return localDB.userDao().getUserById(id)
    }

    fun getJourneyById(id: String, callback: (LiveData<Journey>) -> Unit) {
        executor.execute {
            refreshAllJourneys()
            val journey = localDB.journeyDao().getJourneyById(id)
            mainHandler.post {
                callback(journey)
            }
        }
    }

    fun getPostsByIds(ids: ArrayList<String>, callback: (LiveData<List<Post>>) -> Unit) {
        executor.execute {
            refreshAllPosts()
            val posts = localDB.postDao().getPostsByIds(ids)
            mainHandler.post {
                callback(posts)
            }
        }
    }

    fun uploadImageToFirebase(
        imageUri: Uri,
        onSuccessCallback: ((imagePath: String) -> Unit)? = null,
    ) {
        firebaseModel.uploadImageToFirebase(imageUri, onSuccessCallback)
    }

    fun addUser(user: User, context: Context, callback: () -> Unit) {
        firebaseModel.addUserToDB(user, context) {
            refreshAllUsers()
            callback()
        }
    }

    fun addPost(
        country: String,
        title: String,
        description: String,
        imageURI: String,
        callback: () -> Unit,
    ) {
        executor.execute {
            firebaseModel.addPost(country, title, description, imageURI) {
                refreshAllJourneys()
                refreshAllPosts()
                callback()
            }
        }
    }

    fun updateUserUsername(
        userId: String,
        newUsername: String,
        context: Context,
        callback: () -> Unit
    ) {
        FirebaseModel.updateUserUsername(userId, newUsername, context) { user ->
            executor.execute {
                if (user != null) {
                    localDB.userDao().insert(user)
                }
                mainHandler.post {
                    callback()
                }
            }
            refreshAllUsers()
        }
    }

    fun updateUserProfilePic(userId: String, newImagePath: String, callback: () -> Unit) {
        FirebaseModel.updateUserProfilePic(userId, newImagePath) { user ->
            executor.execute {
                if (user != null) {
                    localDB.userDao().insert(user)
                }
                mainHandler.post {
                    callback()
                }
            }
            refreshAllUsers()
        }
    }

    fun updatePost(
        postId: String,
        title: String,
        description: String,
        imageURI: String,
        callback: () -> Unit,
    ) {
        executor.execute {
            firebaseModel.updatePost(postId, title, description, imageURI) {
                refreshAllJourneys()
                refreshAllPosts()
                callback()
            }
        }
    }

    fun deletePost(post: Post, callback: () -> Unit) {
        firebaseModel.deletePost(post.id) {
            firebaseModel.removePostFromJourney(post.id, post.journeyId) {
                executor.execute {
                    localDB.journeyDao().deleteByJourneyId(post.journeyId)
                    localDB.postDao().delete(post)
                    refreshAllJourneys()
                }
                mainHandler.post {
                    callback()
                }
            }
        }
    }

    private fun refreshAllPosts() {
        val lastUpdate: Long = Post.lastUpdated

        firebaseModel.getAllPostsSince(lastUpdate) { posts ->
            executor.execute {
                var time = lastUpdate
                for (post in posts) {
                    localDB.postDao().insert(post)
                    if (time < post.lastUpdated) {
                        time = post.lastUpdated
                    }
                }
                Post.lastUpdated = time
            }
        }
    }

    private fun refreshAllJourneys() {
        val lastUpdate: Long = Journey.lastUpdated

        firebaseModel.getAllJourneysSince(lastUpdate) { journeys ->
            executor.execute {
                var time = lastUpdate
                for (journey in journeys) {
                    localDB.journeyDao().insert(journey)
                    if (time < journey.lastUpdated) {
                        time = journey.lastUpdated
                    }
                }
                Journey.lastUpdated = time
            }
        }
    }

    private fun refreshAllUsers() {
        val lastUpdate: Long = User.lastUpdated

        firebaseModel.getAllUsersSince(lastUpdate) { users ->
            executor.execute {
                var time = lastUpdate
                for (user in users) {
                    localDB.userDao().insert(user)
                    if (time < user.lastUpdated) {
                        time = user.lastUpdated
                    }
                }
                User.lastUpdated = time
            }
        }
    }
}