package com.example.travelshare.model

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.travelshare.base.MyApplication

@Entity
data class Journey(
    @PrimaryKey val id: String,
    val userId: String,
    val country: String,
    val posts: ArrayList<String>,
    val lastUpdated: Long
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        ArrayList<String>().apply { parcel.readArrayList(String::class.java.classLoader) }
            ?: arrayListOf<String>(),
        parcel.readLong() ?: 0
    )

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(userId)
        dest.writeString(country)
        dest.writeList(posts)
    }

    companion object CREATOR : Parcelable.Creator<Journey> {
        override fun createFromParcel(parcel: Parcel): Journey {
            return Journey(parcel)
        }

        override fun newArray(size: Int): Array<Journey?> {
            return arrayOfNulls(size)
        }

        const val GET_LAST_UPDATED = "get_last_updated"
        var lastUpdated: Long
            get() {
                return MyApplication.Globals
                    .appContext?.getSharedPreferences("TAG", Context.MODE_PRIVATE)
                    ?.getLong(GET_LAST_UPDATED, 0) ?: 0L
            }
            set(value) {
                MyApplication.Globals
                    ?.appContext
                    ?.getSharedPreferences("TAG", Context.MODE_PRIVATE)?.edit()
                    ?.putLong(GET_LAST_UPDATED, value)?.apply()
            }

        fun fromJson(json: MutableMap<String, Any>): Journey {
            val id = json["id"] as? String ?: ""
            val userId = json["userId"] as? String ?: ""
            val country = json["country"] as? String ?: ""
            val posts = json["posts"] as? ArrayList<String> ?: arrayListOf<String>()
            val lastUpdated = json["lastUpdated"] as? Long ?: 0L

            return Journey(id, userId, country, posts, lastUpdated)
        }
    }

    val json: HashMap<String, Any>
        get() {
            return hashMapOf(
                "id" to this.id,
                "userId" to this.userId,
                "country" to this.country,
                "posts" to this.posts,
                "lastUpdated" to this.lastUpdated
            )
        }
}