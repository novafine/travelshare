package com.example.travelshare

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.example.travelshare.converters.JourneyConverter
import com.example.travelshare.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(findViewById(R.id.main_toolbar))
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.logo, null)
        supportActionBar?.setCustomView(view)

        val navHostFragment: NavHostFragment? =
            supportFragmentManager.findFragmentById(R.id.navHostMain) as? NavHostFragment
        navController = navHostFragment?.navController
        navController?.let {
            NavigationUI.setupWithNavController(
                binding.mainActivityBottomNavigationView,
                it
            )
        }

        binding.mainActivityBottomNavigationView.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.journeyListPageFragment,
                R.id.addPostFragment,
                R.id.userPageFragment,
                -> {
                    // TODO: either add back button when needed and uncomment this code or remove commented code
                    // Check if the selected destination is different from the current one
//                    if (binding.mainActivityBottomNavigationView.selectedItemId != item.itemId) {
//                        navController?.popBackStack(
//                            R.id.journeyListPageFragment,
//                            item.itemId == R.id.journeyListPageFragment
//                        )
                        navController?.navigate(item.itemId)
//                    }
                    true
                }

                else -> false
            }
        }
    }
}