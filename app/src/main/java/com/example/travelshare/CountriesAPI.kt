package com.example.travelshare

import android.content.Context
import com.example.travelshare.model.Country
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

object RetrofitClient {
    private const val BASE_URL = "https://restcountries.com/v3.1/"

    val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}

object APIClient {
    private val apiService: ApiService by lazy {
        RetrofitClient.retrofit.create(ApiService::class.java)
    }

    fun getCountriesNames(callback: (List<Country>) -> Unit, context: Context ?= null) {
        val call = this.apiService.getCountriesNames()
        call.enqueue(object: Callback<List<Country>> {
            override fun onResponse(call: Call<List<Country>>, response: Response<List<Country>>) {
                if (response.isSuccessful) {
                    val countries = response.body()

                    if (countries != null) {
                        callback(countries)
                    } else {
                        callback(emptyList())
                        // TODO: Toast error API response body
                    }
                } else {
                    callback(emptyList())
                        // TODO: Toast error API response
                }
            }

            override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                callback(emptyList())
            }
        })
    }
}

private interface ApiService {
    @GET("all?fields=name")
    fun getCountriesNames(): Call<List<Country>>
}