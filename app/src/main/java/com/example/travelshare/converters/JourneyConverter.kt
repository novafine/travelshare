package com.example.travelshare.converters

import androidx.room.TypeConverter
import com.example.travelshare.model.Post
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

object JourneyConverter {
    @TypeConverter
    @JvmStatic
    fun fromPostList(value: ArrayList<String>?): String {
        val gson = Gson()
        return gson.toJson(value)
    }

    @TypeConverter
    @JvmStatic
    fun toPostList(value: String): ArrayList<String>? {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<String>>() {}.type
        return gson.fromJson<ArrayList<String>>(value, type)
    }
}