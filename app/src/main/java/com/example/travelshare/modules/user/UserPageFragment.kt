package com.example.travelshare.modules.user

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.lifecycle.ViewModelProvider
import com.example.travelshare.R
import com.example.travelshare.databinding.FragmentUserPageBinding
import com.example.travelshare.model.FirebaseModel
import com.example.travelshare.model.Model
import com.example.travelshare.model.User
import com.example.travelshare.modules.journeys.JourneysListFragment
import com.example.travelshare.modules.login.LoginActivity
import com.google.android.material.button.MaterialButton
import com.google.android.material.imageview.ShapeableImageView
import com.google.firebase.Firebase
import com.google.firebase.auth.auth
import com.squareup.picasso.Picasso

class UserPageFragment : Fragment() {
    private var _binding: FragmentUserPageBinding? = null
    private val binding get() = _binding!!

    private var userFragmentProfilePic: ShapeableImageView? = null
    private var userFragmentUsername: TextView? = null
    private var userFragmentUserBio: TextView? = null
    private var userFragmentJourneysListContainer: FragmentContainerView? = null
    private var userFragmentJourneyListFragment: JourneysListFragment? = null
    private lateinit var viewModel: UserPageFragmentViewModel
    private var userPageProgressBar: ProgressBar? = null
    private var btnUserPageEditUsername: MaterialButton? = null
    private var isUserLoaded = false
    private var isUserJourneysLoaded = false
    private var user: User? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = FragmentUserPageBinding.inflate(layoutInflater, container, false)
        val view = binding.root
        userFragmentUsername = binding.tvUserFragmentUsername
        userFragmentUsername?.isFocusable = false
        userFragmentUserBio = binding.tvUserFragmentBio
        userFragmentJourneysListContainer = binding.fcUserFragmentJourneys
        userFragmentProfilePic = binding.ivUserFragmentProfile
        viewModel = ViewModelProvider(this)[UserPageFragmentViewModel::class.java]
        userPageProgressBar = binding.userPageProgressBar
        btnUserPageEditUsername = binding.btnUserPageEditUsername
        userPageProgressBar?.visibility = View.VISIBLE


        Firebase.auth.currentUser?.uid?.let {
            viewModel.myJourneys = Model.instance.getJourneysByUserId(it)
            FirebaseModel.getUserById(it) { user ->
                this.user = user
                initUser(user)
                isUserLoaded = true
                checkLoadingState()
            }
        }
        initUserJourneyList()

        binding.ibLogout.setOnClickListener {
            logout()
        }

        handleUsernameEdit()
        handleProfilePictureEdit()

        return view
    }

    private fun initUser(user: User) {
        userFragmentUsername?.text = "${user.username}"
        userFragmentUserBio?.text = user.bio ?: "bio"
        Picasso.get().load(user.imagePath).into(userFragmentProfilePic)
    }

    private fun initUserJourneyList() {
        viewModel.myJourneys?.observe(viewLifecycleOwner) { userJourneys ->
            val userJourneysArrayList = ArrayList(userJourneys)
            userFragmentJourneyListFragment =
                JourneysListFragment.newInstance(userJourneysArrayList)
            userFragmentJourneyListFragment?.let { fragment ->
                val transaction = getChildFragmentManager().beginTransaction()
                transaction.replace(R.id.fcUserFragmentJourneys, fragment)
                transaction.commit()
            }
            isUserJourneysLoaded = true
            checkLoadingState()
        }
    }

    private fun checkLoadingState() {
        if (isUserLoaded && isUserJourneysLoaded) {
            userPageProgressBar?.visibility = View.GONE
        }
    }

    private fun handleUsernameEdit() {
        btnUserPageEditUsername?.setOnClickListener {
            if (userFragmentUsername?.isFocusable == true) {
                val newUsername = userFragmentUsername?.text.toString()
                updateUsername(newUsername)
            } else {
                userFragmentUsername?.isEnabled = true
                userFragmentUsername?.isFocusable = true
                userFragmentUsername?.isFocusableInTouchMode = true
                userFragmentUsername?.requestFocus()
                btnUserPageEditUsername?.setIconResource(R.drawable.baseline_check_24)
            }
        }
    }

    private fun updateUsername(newUsername: String) {
        userFragmentUsername?.isFocusable = false
        userFragmentUsername?.isFocusableInTouchMode = false
        userFragmentUsername?.isEnabled = false
        btnUserPageEditUsername?.setIconResource(R.drawable.baseline_edit_24)
        user?.let {
            Model.instance.updateUserUsername(it.id, newUsername, requireContext()) {
                updateUser(it.id)
            }
        }
    }

    private fun updateJourneyList() {
        viewModel.myJourneys?.observe(viewLifecycleOwner) { userJourneys ->
            val userJourneysArrayList = ArrayList(userJourneys)
            userFragmentJourneyListFragment?.updateJourneys(userJourneysArrayList)
        }
    }

    private fun handleProfilePictureEdit() {
        val pickMedia =
            registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
                if (uri != null) {
                    Log.d("PhotoPicker", "Selected URI: $uri")
                    userFragmentProfilePic?.setImageResource(R.color.imageBackground)
                    userFragmentProfilePic?.setBackgroundResource(R.color.imageBackground)
                    Model.instance.uploadImageToFirebase(uri) { imagePath ->
                        user?.let {
                            Model.instance.updateUserProfilePic(it.id, imagePath) {
                                updateUser(it.id)
                            }
                        }

                    }
                } else {
                    Log.d("PhotoPicker", "No media selected")
                }
            }

        userFragmentProfilePic?.setOnClickListener {
            pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }
    }

    private fun updateUser(userId: String) {
        Model.instance.getUserById(userId).observe(viewLifecycleOwner) { user ->
            initUser(user)
            updateJourneyList()
        }
    }

    private fun logout() {
        Firebase.auth.signOut()
        activity?.let {
            val loginIntent = Intent(it, LoginActivity::class.java)
            it.startActivity(loginIntent)
            it.finish()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}