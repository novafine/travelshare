package com.example.travelshare.modules.journeys.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.travelshare.R
import com.example.travelshare.model.Post
import com.example.travelshare.modules.journeys.JourneyPageFragment

class JourneyPostRecyclerViewAdapter(private var posts: List<Post>?): RecyclerView.Adapter<JourneyPostViewHolder>() {
    var listener: JourneyPageFragment.OnItemClickListener? = null

    override fun getItemCount(): Int = this.posts?.size ?: 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JourneyPostViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.journy_post_grid_item, parent, false)
        return JourneyPostViewHolder(itemView, listener)
    }

    override fun onBindViewHolder(holder: JourneyPostViewHolder, position: Int) {
        val post = this.posts?.get(position)
        holder.bind(post)
    }

    fun updateData(newData: ArrayList<Post>) {
        posts = newData
        notifyDataSetChanged()
    }


}