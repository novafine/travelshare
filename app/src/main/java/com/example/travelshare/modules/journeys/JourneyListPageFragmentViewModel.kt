package com.example.travelshare.modules.journeys

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.travelshare.model.Journey

class JourneyListPageFragmentViewModel: ViewModel() {
    var journeys: LiveData<List<Journey>>? = null
}