package com.example.travelshare.modules.journeys

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.FragmentContainerView
import androidx.lifecycle.ViewModelProvider
import com.example.travelshare.R
import com.example.travelshare.model.Model

/**
 * A simple [Fragment] subclass.
 * Use the [JourneyListPageFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class JourneyListPageFragment : Fragment() {

    private var journeyListContainer: FragmentContainerView? = null
    private var journeyListFragment: JourneysListFragment? = null
    private lateinit var viewModel: JourneyListPageFragmentViewModel
    private var journeyListPageProgressBar: ProgressBar? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_journey_list_page, container, false)
        viewModel = ViewModelProvider(this)[JourneyListPageFragmentViewModel::class.java]
        viewModel.journeys = Model.instance.getAllJourneys()
        journeyListContainer = view.findViewById(R.id.fcJourneyListPageJourneyList)
        journeyListPageProgressBar = view.findViewById(R.id.journeyListPageProgressBar)
        journeyListPageProgressBar?.visibility = View.VISIBLE

        initJourneyList()

        return view
    }

    private fun initJourneyList() {
        viewModel.journeys?.observe(viewLifecycleOwner) { allJourneys ->
            val allJourneysArrayList = ArrayList(allJourneys)
            journeyListPageProgressBar?.visibility = View.GONE
            journeyListFragment = JourneysListFragment.newInstance(allJourneysArrayList)
            journeyListFragment?.let { fragment ->
                val transaction = getChildFragmentManager().beginTransaction()
                transaction.replace(R.id.fcJourneyListPageJourneyList, fragment)
                transaction.commit()
            }
        }
    }
}