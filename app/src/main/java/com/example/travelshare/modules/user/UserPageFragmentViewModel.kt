package com.example.travelshare.modules.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.travelshare.model.Journey

class UserPageFragmentViewModel : ViewModel() {
    var myJourneys: LiveData<List<Journey>>? = null
}