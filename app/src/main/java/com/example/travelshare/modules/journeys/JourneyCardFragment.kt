package com.example.travelshare.modules.journeys

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.travelshare.R
import com.example.travelshare.model.FirebaseModel
import com.example.travelshare.model.Journey
import com.google.android.material.imageview.ShapeableImageView
import com.squareup.picasso.Picasso

/**
 * A simple [Fragment] subclass.
 * Use the [JourneyCardFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class JourneyCardFragment : Fragment() {

    private var journey: Journey? = null
    private var journeyCardHeader: TextView? = null
    private var journeyCardCountry: TextView? = null
    private var journeyCardProgressBar: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            journey = it.getParcelable(JOURNEY)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_journey_card, container, false)
        journeyCardHeader = view.findViewById(R.id.tvJourneyCardFragmentHeader)
        journeyCardCountry = view.findViewById(R.id.tvJourneyCardFragmentCountry)
        journeyCardProgressBar = view.findViewById(R.id.journeyCardProgressBar)
        journeyCardProgressBar?.visibility = View.VISIBLE
        val journeyCardProfilePic =
            view.findViewById<ShapeableImageView>(R.id.ivJourneyCardFragmentProfile)
        journeyCardProfilePic.setBackgroundResource(R.drawable.gardient_backgroud)
        journey?.userId?.let {
            FirebaseModel.getUserById(it) { user ->
                Picasso.get().load(user.imagePath).into(journeyCardProfilePic)
                journeyCardHeader?.text = "${user.username}'s journey to"
                journeyCardCountry?.text = journey?.country ?: "country"
                journeyCardProgressBar?.visibility = View.GONE
            }
        }
        return view
    }

    companion object {
        const val JOURNEY = "journey"

        fun newInstance(journey: Journey) =
            JourneyCardFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(JOURNEY, journey);
                }
            }
    }
}