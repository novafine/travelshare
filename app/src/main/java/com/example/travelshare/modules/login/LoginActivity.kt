package com.example.travelshare.modules.login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import androidx.appcompat.app.AppCompatActivity
import com.example.travelshare.MainActivity
import com.example.travelshare.databinding.ActivityLoginBinding
import com.example.travelshare.modules.signup.SignupActivity
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth


class LoginActivity : AppCompatActivity() {
    private var _binding: ActivityLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Firebase.auth.currentUser != null) {
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        auth = Firebase.auth
        _binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setFormValidators()

        binding.btnLogin.setOnClickListener {
            if (this.validateForm())
                this.signIn(
                    binding.tfLoginEmail.editText?.text.toString(),
                    binding.tfLoginPassword.editText?.text.toString()
                )
        }

        binding.tvGoToSignup.setOnClickListener {
            this.navigateToSignup()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun validateForm(): Boolean {
        val isEmailValid = validateEmail(binding.tfLoginEmail)
        val isPasswordValid = validatePassword(binding.tfLoginPassword)

        return isEmailValid && isPasswordValid
    }

    private fun setFormValidators() {
        binding.tfLoginEmail.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                str: CharSequence?,
                start: Int,
                count: Int,
                after: Int,
            ) {
            }

            override fun onTextChanged(str: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(str: Editable?) {
                this@LoginActivity.validateEmail(binding.tfLoginEmail)
            }
        })

        binding.tfLoginPassword.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                str: CharSequence?,
                start: Int,
                count: Int,
                after: Int,
            ) {
            }

            override fun onTextChanged(str: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(str: Editable?) {
                this@LoginActivity.validatePassword(binding.tfLoginPassword)
            }
        })
    }

    private fun validateEmail(email: com.google.android.material.textfield.TextInputLayout): Boolean {
        if (email.editText?.text.toString().trim().isEmpty()) {
            email.error = "email is required"
            return false
        }

        val isValid = Patterns.EMAIL_ADDRESS.matcher(email.editText?.text.toString()).matches()

        if (!isValid) {
            email.error = "Invalid Email address, ex: abc@example.com"
            return false
        }

        email.error = null
        return true
    }

    private fun validatePassword(password: com.google.android.material.textfield.TextInputLayout): Boolean {
        if (password.editText?.text.toString().trim().isEmpty()) {
            password.error = "Password is required"
            return false
        }

        if (password.editText?.text.toString().length < 6) {
            password.error = "Password length can't be less than 6 characters"
            return false
        }

        password.error = null
        return true
    }

    private fun signIn(email: String, password: String) {
        Log.d("login", "signIn:$email")

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("login", "signInWithEmail:success  " + auth.currentUser)
                    navigateToMainActivity()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("login", "signInWithEmail:failure", task.exception)
//                    Toast.makeText(
//                        context,
//                        "Authentication failed.",
//                        Toast.LENGTH_SHORT,
//                    ).show()
//                    updateUI(null)
                }
            }
    }

    private fun navigateToSignup() {
        val intent = Intent(this, SignupActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun navigateToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}