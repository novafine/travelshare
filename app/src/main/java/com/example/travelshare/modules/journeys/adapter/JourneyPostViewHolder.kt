package com.example.travelshare.modules.journeys.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.travelshare.R
import com.example.travelshare.model.Post
import com.example.travelshare.modules.journeys.JourneyPageFragment
import com.squareup.picasso.Picasso

class JourneyPostViewHolder(
    private val itemView: View,
    private val listener: JourneyPageFragment.OnItemClickListener?,
) : RecyclerView.ViewHolder(itemView) {

    private var ivJourneyPostGridItemImage: ImageView? = null
    private var tvPostGridItemTitle: TextView? = null
    private var post: Post? = null

    init {
        ivJourneyPostGridItemImage = itemView.findViewById(R.id.ivJourneyPostGridItemImage)
        tvPostGridItemTitle = itemView.findViewById(R.id.tvPostGridItemTitle)

        itemView.setOnClickListener {
            listener?.onItemClicked(adapterPosition)
        }
    }

    fun bind(post: Post?) {
        this.post = post
        if (post?.imageURI == null || post.imageURI == "") {
            ivJourneyPostGridItemImage?.setImageResource(R.drawable.baseline_add_photo_alternate_24)
        } else {
            Picasso.get().load(post.imageURI).into(ivJourneyPostGridItemImage)
        }
        tvPostGridItemTitle?.text = post?.title ?: "post title"
    }
}