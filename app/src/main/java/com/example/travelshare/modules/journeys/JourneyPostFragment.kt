package com.example.travelshare.modules.journeys

import android.net.Uri
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.navigation.Navigation
import com.example.travelshare.R
import com.example.travelshare.databinding.FragmentJourneyPostBinding
import com.example.travelshare.model.Journey
import com.example.travelshare.model.Model
import com.example.travelshare.model.Post
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.Firebase
import com.google.firebase.auth.auth
import com.squareup.picasso.Picasso

class JourneyPostFragment : Fragment() {

    private var _binding: FragmentJourneyPostBinding? = null
    private val binding get() = _binding!!
    private var post: Post? = null
    private var journey: Journey? = null
    private var journeyCardFragment: JourneyCardFragment? = null
    private var fcJourneyPostJourneyCardFragment: FragmentContainerView? = null
    private var ivJourneyPostImage: ImageView? = null
    private var tvJourneyPostTitle: TextView? = null
    private var tvJourneyPostDescription: TextView? = null
    private var buttonsContainer: ConstraintLayout? = null
    private var tfEditPostTitle: TextInputLayout? = null
    private var tfEditDescription: TextInputLayout? = null
    private var etEditPostTitle: TextInputEditText? = null
    private var btnPostPageEditPost: MaterialButton? = null
    private var isEditMode = false
    private var imageURI: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            post = it.getParcelable(POST)
            journey = it.getParcelable(JOURNEY)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = FragmentJourneyPostBinding.inflate(layoutInflater, container, false)
        val view = binding.root

        fcJourneyPostJourneyCardFragment = binding.fcJourneyPostJourneyCardFragment
        ivJourneyPostImage = binding.ivJourneyPostImage
        tvJourneyPostTitle = binding.tvJourneyPostTitle
        tvJourneyPostDescription = binding.tvJourneyPostDescription
        buttonsContainer = binding.buttonsContainer

        tfEditPostTitle = binding.tfEditPostTitle
        tfEditDescription = binding.tfEditDescription
        etEditPostTitle = binding.etEditPostTitle
        btnPostPageEditPost = binding.btnPostPageEditPost

        tvJourneyPostDescription!!.movementMethod = ScrollingMovementMethod()

        initJourneyCard()
        initPost()
        initActionButtons()

        val pickMedia =
            registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
                if (uri != null) {
                    Log.d("PhotoPicker", "Selected URI: $uri")
                    ivJourneyPostImage?.setImageURI(uri)
                    this.imageURI = uri
                } else {
                    Log.d("PhotoPicker", "No media selected")
                }
            }
        binding.ivJourneyPostImage.setOnClickListener {
            if (this.isEditMode)
                pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }

        return view
    }

    private fun initJourneyCard() {
        journeyCardFragment = journey?.let { JourneyCardFragment.newInstance(it) }
        journeyCardFragment?.let { fragment ->
            val transaction = getChildFragmentManager().beginTransaction()
            transaction.replace(R.id.fcJourneyPostJourneyCardFragment, fragment)
            transaction.commit()
        }
    }

    private fun initPost() {
        Log.d("viewPost", "$post")

        if (post?.imageURI == null || post?.imageURI == "") {
            ivJourneyPostImage?.setImageResource(R.drawable.baseline_add_photo_alternate_24)
        } else {
            Picasso.get().load(post?.imageURI).into(ivJourneyPostImage)
        }
        tvJourneyPostTitle?.text = post?.title
        tvJourneyPostDescription?.text = post?.description
        etEditPostTitle?.setText(post?.title)
        tvJourneyPostDescription?.text = post?.description
    }

    private fun toggleEditMode() {
        this.isEditMode = !this.isEditMode

        if (isEditMode) {
            this.switchToEditMode()
        } else {
            this.switchToViewMode()
            this.updatePost()
        }
    }

    private fun switchToEditMode() {
        btnPostPageEditPost?.setIconResource(R.drawable.baseline_check_24)
        ivJourneyPostImage?.setBackgroundResource(R.color.primaryAccent)
        tvJourneyPostTitle?.visibility = View.INVISIBLE
        tfEditPostTitle?.visibility = View.VISIBLE
        etEditPostTitle?.setText(tvJourneyPostTitle?.text)

        tvJourneyPostDescription?.visibility = View.INVISIBLE
        tfEditDescription?.visibility = View.VISIBLE
        tfEditDescription?.editText?.setText(tvJourneyPostDescription?.text)
    }

    private fun switchToViewMode() {
        btnPostPageEditPost?.setIconResource(R.drawable.baseline_edit_24)
        ivJourneyPostImage?.setBackgroundResource(R.color.imageBackground)
        tvJourneyPostTitle?.visibility = View.VISIBLE
        tfEditPostTitle?.visibility = View.INVISIBLE
        tvJourneyPostTitle?.text = etEditPostTitle?.text.toString()

        tvJourneyPostDescription?.visibility = View.VISIBLE
        tfEditDescription?.visibility = View.INVISIBLE
        tvJourneyPostDescription?.text = tfEditDescription?.editText?.text.toString()
    }

    private fun updatePost() {
        var imagePath = post?.imageURI
        val update = {
            post?.id.let {
                imagePath?.let { uri ->
                    Model.instance.updatePost(
                        post?.id!!,
                        etEditPostTitle?.text.toString(),
                        tfEditDescription?.editText?.text.toString(),
                        uri
                    ) {
                        Log.d("updatePost", "post ${post?.id} updated successfully")
                    }
                }
            }
        }
        if (this.imageURI != null) {
            Model.instance.uploadImageToFirebase(this.imageURI!!) { it ->
                imagePath = it
                update()
            }
        } else {
            update()
        }
    }

    private fun initActionButtons() {
        if (journey?.userId == Firebase.auth.currentUser?.uid) {
            buttonsContainer?.visibility = View.VISIBLE
            val btnPostPageEditPost = binding.btnPostPageEditPost
            val btnPostPageDeletePost = binding.btnPostPageDeletePost

            btnPostPageEditPost.setOnClickListener {
                toggleEditMode()
            }

            btnPostPageDeletePost.setOnClickListener { view ->
                MaterialAlertDialogBuilder(view.context)
                    .setTitle("Delete Post?")
                    .setMessage("This post will be permanently deleted")
                    .setNegativeButton("cancel") { dialog, which ->
                    }
                    .setPositiveButton("delete") { dialog, which ->
                        post?.let {
                            Model.instance.deletePost(it) {
                                Log.d("deletePost", "post $post deleted successfully")
                                Navigation.findNavController(view).popBackStack()
                            }
                        }
                    }
                    .show()
            }
        } else {
            buttonsContainer?.visibility = View.GONE
        }
    }

    companion object {

        var POST = "post"
        var JOURNEY = "journey"
        fun newInstance(post: Post, journey: Journey) =
            JourneyPostFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(POST, post)
                    putParcelable(JOURNEY, journey)
                }
            }
    }
}