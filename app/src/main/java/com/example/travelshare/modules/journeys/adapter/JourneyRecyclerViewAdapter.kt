package com.example.travelshare.modules.journeys.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.travelshare.R
import com.example.travelshare.model.Journey
import com.example.travelshare.modules.journeys.JourneysListFragment

class JourneyRecyclerViewAdapter(var journeys: List<Journey>?): RecyclerView.Adapter<JourneyCardViewHolder>() {
    var listener: JourneysListFragment.OnItemClickListener? = null

    override fun getItemCount(): Int = journeys?.size ?: 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JourneyCardViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.journey_card_row, parent, false)
        return JourneyCardViewHolder(itemView, listener)
    }

    override fun onBindViewHolder(holder: JourneyCardViewHolder, position: Int) {
        val journey = journeys?.get(position)
        holder.bind(journey)
    }

    fun updateData(newData: ArrayList<Journey>) {
        journeys = emptyList()
        journeys = newData.toList()
        notifyDataSetChanged()
    }
}