package com.example.travelshare.modules.signup

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.example.travelshare.R
import com.example.travelshare.databinding.ActivitySignupBinding
import com.example.travelshare.model.FirebaseModel
import com.example.travelshare.model.FirebaseModel.isUsernameExists
import com.example.travelshare.model.Model
import com.example.travelshare.model.User
import com.example.travelshare.modules.login.LoginActivity
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth

class SignupActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private var _binding: ActivitySignupBinding? = null
    private val binding get() = _binding!!
    private var imageURI: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
        _binding = ActivitySignupBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setFormValidators()

        val pickMedia =
            registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
                if (uri != null) {
                    Log.d("PhotoPicker", "Selected URI: $uri")
                    binding.ivProfilePicture.setImageURI(uri)
                    binding.ivProfilePicture.setBackgroundResource(R.color.imageBackground)
                    this.imageURI = uri
                } else {
                    Log.d("PhotoPicker", "No media selected")
                }
            }

        binding.ivProfilePicture.setOnClickListener {
            pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }

        binding.btnSignup.setOnClickListener {
            validateForm() {
                if (it) {
                    this.register()
                }
            }

        }

        binding.tvGoToLogin.setOnClickListener {
            this.navigateToLogin()
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser

        if (currentUser != null) {
            Log.d("signup", currentUser.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun validateForm(callback: (isUsernameValid: Boolean) -> Unit) {
        validateUsername(binding.tfSignupUsername) {
            val isEmailValid = validateEmail(binding.tfSignupEmail)
            val isPasswordValid = validatePassword(binding.tfSignupPassword)
            val isBioValid = validateBio(binding.tfSignupBio)
            val isUserImageValid = validateUserImage(this.imageURI)
            val isFormValid =
                isEmailValid && isPasswordValid && isBioValid && isUserImageValid && it
            callback(isFormValid)
        }
    }

    private fun setFormValidators() {
        binding.tfSignupEmail.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                str: CharSequence?,
                start: Int,
                count: Int,
                after: Int,
            ) {
            }

            override fun onTextChanged(str: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(str: Editable?) {
                this@SignupActivity.validateEmail(binding.tfSignupEmail)
            }
        })

        binding.tfSignupPassword.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                str: CharSequence?,
                start: Int,
                count: Int,
                after: Int,
            ) {
            }

            override fun onTextChanged(str: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(str: Editable?) {
                this@SignupActivity.validatePassword(binding.tfSignupPassword)
            }
        })

        binding.tfSignupUsername.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                str: CharSequence?,
                start: Int,
                count: Int,
                after: Int,
            ) {
            }

            override fun onTextChanged(str: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(str: Editable?) {
                this@SignupActivity.validateUsername(binding.tfSignupUsername) {}
            }
        })

        binding.tfSignupBio.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                str: CharSequence?,
                start: Int,
                count: Int,
                after: Int,
            ) {
            }

            override fun onTextChanged(str: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(str: Editable?) {
                this@SignupActivity.validateBio(binding.tfSignupBio)
            }
        })
    }

    private fun validateEmail(email: com.google.android.material.textfield.TextInputLayout): Boolean {
        if (email.editText?.text.toString().trim().isEmpty()) {
            email.error = "email is required"
            return false
        }

        val isValid = Patterns.EMAIL_ADDRESS.matcher(email.editText?.text.toString()).matches()

        if (!isValid) {
            email.error = "Invalid Email address, ex: abc@example.com"
            return false
        }

        email.error = null
        return true
    }

    private fun validatePassword(password: com.google.android.material.textfield.TextInputLayout): Boolean {
        if (password.editText?.text.toString().trim().isEmpty()) {
            password.error = "Password is required"
            return false
        }

        if (password.editText?.text.toString().length < 6) {
            password.error = "Password length can't be less than 6 characters"
            return false
        }

        password.error = null
        return true
    }

    private fun validateUsername(
        username: com.google.android.material.textfield.TextInputLayout,
        callback: (isUsernameValid: Boolean) -> Unit
    ) {
        username.error = null
        if (username.editText?.text.toString().trim().isEmpty()) {
            username.error = "Username is required"
            callback(false)
        } else {
            if (username.editText?.text.toString().length < 4) {
                username.error = "Username length can't be less than 4 characters"
                callback(false)
            } else {
                isUsernameExists(username.editText?.text.toString()) {
                    if (it) {
                        FirebaseModel.showToast(this, "Username is already taken")
                        callback(false)
                    } else {
                        callback(true)
                    }
                }
            }
        }
    }

    private fun validateBio(bio: com.google.android.material.textfield.TextInputLayout): Boolean {
        if (bio.editText?.text.toString().trim().isEmpty()) {
            bio.error = "Bio is required"
            return false
        }

        bio.error = null
        return true
    }

    private fun validateUserImage(imageUri: Uri?): Boolean {
        if (imageUri == null) {
            binding.ivProfilePicture.setBackgroundResource(R.color.error)
            // TODO: Toast select profile picture
            return false
        }

        return true
    }

    private fun createAccount(email: String, password: String, callback: (userId: String) -> Unit) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("signup", "createUserWithEmail:success")
                    val user = auth.currentUser
                    user?.let { callback(it.uid) }
                } else {
                    Log.w(
                        "signup",
                        "createUserWithEmail:failure" + task.exception?.message,
                        task.exception
                    )
                    Toast.makeText(
                        baseContext,
                        "Authentication failed - " + task.exception?.message,
                        Toast.LENGTH_SHORT,
                    ).show()
                }
            }
    }

    private fun register() {
        this.createAccount(
            binding.etSignupEmail.text.toString(),
            binding.etSignupPassword.text.toString()
        ) { userId ->
            this.imageURI?.let {
                Model.instance.uploadImageToFirebase(it) { imagePath ->
                    val user = User(
                        userId, binding.tfSignupUsername.editText!!.text.toString(),
                        binding.tfSignupBio.editText!!.text.toString(),
                        imagePath,
                        System.currentTimeMillis()
                    )
                    Model.instance.addUser(user, this) {
                        Log.d("signup", "addUser successfully")
                        navigateToLogin()
                    }
                }
            }
        }
    }

    private fun navigateToLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}