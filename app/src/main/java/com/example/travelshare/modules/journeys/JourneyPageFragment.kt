package com.example.travelshare.modules.journeys

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.travelshare.R
import com.example.travelshare.model.Journey
import com.example.travelshare.model.Model
import com.example.travelshare.modules.journeys.adapter.JourneyPostRecyclerViewAdapter

/**
 * A simple [Fragment] subclass.
 * Use the [JourneyPageFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class JourneyPageFragment : Fragment() {
    private var journeyId: String? = null
    private var fcJourneyPageJourneyCardContainer: FragmentContainerView? = null
    private var rvJourneyPagePostGridList: RecyclerView? = null
    private var journeyCardFragment: JourneyCardFragment? = null
    private var adapter: JourneyPostRecyclerViewAdapter =
        JourneyPostRecyclerViewAdapter(ArrayList())
    private var journeyPageProgressBar: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            journeyId = it.getString(JOURNEY_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        val view = inflater.inflate(R.layout.fragment_journey_page, container, false)
        fcJourneyPageJourneyCardContainer = view.findViewById(R.id.fcJourneyPageJournyCardContainer)
        rvJourneyPagePostGridList = view.findViewById(R.id.rvJourneyPagePostGridList)
        rvJourneyPagePostGridList?.setHasFixedSize(true)
        journeyPageProgressBar = view.findViewById(R.id.journeyPageProgressBar)
        rvJourneyPagePostGridList?.adapter = adapter
        rvJourneyPagePostGridList?.layoutManager = GridLayoutManager(this.context, 2)

        journeyPageProgressBar?.visibility = View.VISIBLE

        journeyId?.let { journeyId ->
            Model.instance.getJourneyById(journeyId) { journeyLiveData ->
                journeyLiveData.observe(viewLifecycleOwner) { journey ->
                    if (journey != null) {
                        initJourneyCard(journey)
                        initAdapter(journey)
                    } else {
                        Navigation.findNavController(view).popBackStack()
                    }
                }
            }
        }

        return view
    }

    interface OnItemClickListener {
        fun onItemClicked(position: Int)
    }

    private fun initJourneyCard(journey: Journey) {
        journeyCardFragment = JourneyCardFragment.newInstance(journey)
        journeyCardFragment?.let { fragment ->
            val transaction = getChildFragmentManager().beginTransaction()
            transaction.replace(R.id.fcJourneyPageJournyCardContainer, fragment)
            transaction.commit()
        }
    }

    private fun initAdapter(journey: Journey) {
        Model.instance.getPostsByIds(journey.posts) { journeyPostsLiveData ->
            journeyPostsLiveData.observe(viewLifecycleOwner) { journeyPosts ->
                val journeyPostsArrayList = ArrayList(journeyPosts)
                adapter.updateData(journeyPostsArrayList)
                adapter.listener = object : OnItemClickListener {
                    override fun onItemClicked(position: Int) {
                        val clickedPost = journeyPostsArrayList[position]
                        val action =
                            JourneyPageFragmentDirections.actionJourneyPageFragmentToJourneyPostFragment(
                                journey,
                                clickedPost
                            )
                        findNavController().navigate(action)
                    }
                }
            }
        }
        journeyPageProgressBar?.visibility = View.GONE
    }

    companion object {
        var JOURNEY_ID = "journeyId"
        fun newInstance(journeyId: String) =
            JourneyPageFragment().apply {
                arguments = Bundle().apply {
                    putString(JOURNEY_ID, journeyId)
                }
            }
    }
}