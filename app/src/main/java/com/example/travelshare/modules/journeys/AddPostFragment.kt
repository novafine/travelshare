package com.example.travelshare.modules.journeys

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.travelshare.APIClient.getCountriesNames
import com.example.travelshare.R
import com.example.travelshare.databinding.FragmentAddPostBinding
import com.example.travelshare.model.Country
import com.example.travelshare.model.Model
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import com.google.android.material.textfield.TextInputLayout

class AddPostFragment : Fragment() {
    private var _binding: FragmentAddPostBinding? = null
    private val binding get() = _binding!!
    private var imageURI: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAddPostBinding.inflate(layoutInflater, container, false)
        val view = binding.root

        val setCountriesList: (items: List<Country>) -> Unit = { countries ->
            val countriesNames = countries.map { it.name.common }.toTypedArray().sortedArray()
            (binding.tvSelectCountry as? MaterialAutoCompleteTextView)?.setSimpleItems(
                countriesNames
            )
        }
        getCountriesNames(setCountriesList)

        val pickMedia =
            registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
                if (uri != null) {
                    Log.d("PhotoPicker", "Selected URI: $uri")
                    binding.ivAddImage.setImageURI(uri)
                    binding.ivAddImage.setBackgroundResource(R.color.imageBackground)
                    this.imageURI = uri
                } else {
                    Log.d("PhotoPicker", "No media selected")
                }
            }
        binding.ivAddImage.setOnClickListener {
            pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }

        binding.tvSelectCountry.setOnItemClickListener { _, _, _, _ ->
            this@AddPostFragment.validateSelectCountry(binding.tvSelectCountry)
        }

        binding.tfAddPostTitle.editText?.addTextChangedListener {
            this@AddPostFragment.validateTitle(binding.tfAddPostTitle)
        }

        binding.ivUploadPost.setOnClickListener { uploadPostView ->
            if (this.isPostValid()) {
                //TODO: Add to DB etc
                Model.instance.uploadImageToFirebase(this.imageURI!!) { imagePath ->
                    Model.instance.addPost(
                        binding.tvSelectCountry.editableText.toString(),
                        binding.tfAddPostTitle.editText?.text.toString(),
                        binding.tfAddDescription.editText?.text.toString(),
                        imagePath
                    ) {
                        Log.d("add", "post added successfully")
                    }
                    Navigation.findNavController(uploadPostView).navigate(R.id.userPageFragment)
                    Navigation.findNavController(uploadPostView)
                        .popBackStack(R.id.userPageFragment, false)
                }
                Log.d("add", "ok")
            }
        }

        return view
    }

    private fun isPostValid(): Boolean {
        val isSelectCountryValid = this.validateSelectCountry(binding.tvSelectCountry)
        val isTitleValid = this.validateTitle(binding.tfAddPostTitle)
        val isImageValid = this.validateImage(this.imageURI)

        return isSelectCountryValid && isTitleValid && isImageValid
    }

    private fun validateSelectCountry(selectCountry: MaterialAutoCompleteTextView): Boolean {
        if (selectCountry.text.toString().trim().isEmpty()) {
            selectCountry.error = "country is required"
            return false
        }

        selectCountry.error = null
        return true
    }

    private fun validateTitle(title: TextInputLayout): Boolean {
        if (title.editText?.text.toString().trim().isEmpty()) {
            title.error = "title is required"
            return false
        }

        if (title.editText?.text.toString().length > 20) {
            title.error = "Max title length is 20 characters"
            return false
        }

        title.error = null
        return true
    }

    private fun validateImage(imageUri: Uri?): Boolean {
        if (imageUri == null) {
            binding.ivAddImage.setBackgroundResource(R.color.error)
            // TODO: Toast select profile picture
            return false
        }

        return true
    }
}

