package com.example.travelshare.modules.journeys

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.travelshare.R
import com.example.travelshare.model.Journey
import com.example.travelshare.modules.journeys.adapter.JourneyRecyclerViewAdapter

class JourneysListFragment : Fragment() {

    private var journeyListRecyclerView: RecyclerView? = null
    var journeys: ArrayList<Journey>? = null
    private var adapter: JourneyRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            journeys = it.getParcelableArrayList(JOURNEYS)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_journeys_list, container, false)

        journeyListRecyclerView = view.findViewById(R.id.rvJourneyCardsList)
        journeyListRecyclerView?.setHasFixedSize(true)
        journeyListRecyclerView?.layoutManager = LinearLayoutManager(this.context)

        initAdapter()

        return view
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    private fun initAdapter() {
        adapter = JourneyRecyclerViewAdapter(journeys)
        adapter?.listener = object : OnItemClickListener {
            override fun onItemClick(position: Int) {
                journeys?.get(position)?.let { journey ->
                    val action = JourneysListFragmentDirections
                        .actionGlobalJourneyPageFragment(journey.id)
                    findNavController().navigate(action)
                }
            }
        }
        journeyListRecyclerView?.adapter = adapter
    }

    fun updateJourneys(newJourneys: ArrayList<Journey>) {
            adapter?.updateData(newJourneys)
    }

    companion object {

        const val JOURNEYS = "journeys"

        fun newInstance(journeys: ArrayList<Journey>) =
            JourneysListFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(JOURNEYS, journeys)
                }
            }
    }
}