package com.example.travelshare.modules.journeys.adapter

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.travelshare.R
import com.example.travelshare.model.FirebaseModel
import com.example.travelshare.model.Journey
import com.example.travelshare.model.Model
import com.example.travelshare.modules.journeys.JourneysListFragment
import com.google.android.material.imageview.ShapeableImageView
import com.squareup.picasso.Picasso

class JourneyCardViewHolder(
    private val itemview: View,
    private val listener: JourneysListFragment.OnItemClickListener?,
) : RecyclerView.ViewHolder(itemview) {

    private var journeyHeaderTextView: TextView? = null
    private var journeyCountryTextView: TextView? = null
    private var journey: Journey? = null
    private var journeyUserProfilePic: ShapeableImageView? = null
    private var journeyCardRowProgressBar: ProgressBar? = null

    init {
        journeyHeaderTextView = itemView.findViewById(R.id.tvJourneyCardRowHeader)
        journeyCountryTextView = itemView.findViewById(R.id.tvJourneyCardRowCountry)
        journeyCardRowProgressBar = itemview.findViewById(R.id.journeyCardRowProgressBar)
        journeyCardRowProgressBar?.visibility = View.VISIBLE

        journeyUserProfilePic = itemView.findViewById(R.id.ivJourneyCardRowProfile)
        journeyUserProfilePic?.setBackgroundResource(R.drawable.gardient_backgroud)

        itemView.setOnClickListener {
            listener?.onItemClick(adapterPosition)
        }
    }

    fun bind(journey: Journey?) {
        this.journey = journey
        journey?.let {
            FirebaseModel.getUserById(it.userId) { user ->
                journeyHeaderTextView?.text = "${user.username}'s journey to"
                Picasso.get().load(user.imagePath).into(journeyUserProfilePic)
                journeyCountryTextView?.text = journey.country
                journeyCardRowProgressBar?.visibility = View.GONE
            }
        }
    }
}